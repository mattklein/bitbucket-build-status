"""
The primary Flask application.

This application acts as a facade over the BitBucket build status API,
and supports updating build status directly as well as via webhooks from
the Codeship continuous integration service.
"""

import os

from flask import *
from pybitbucket.auth import OAuth1Authenticator
from pybitbucket.bitbucket import Client
from pybitbucket.build import BuildStatus, BuildStatusStates

from utils import *

# the flask application
app = Flask(__name__)

# the bitbucket client, we use OAuth1 with key/secret
client = Client(OAuth1Authenticator(
  client_key=os.environ.get("BITBUCKET_API_KEY"),
  client_secret=os.environ.get("BITBUCKET_API_SECRET")
))


@app.route("/<key>/update/<owner>/<repository_name>/<commit_hash>", methods=["PUT"])
@produces_json
def update_build_status(key, owner, repository_name, commit_hash):
    """
    Updates the build status for the given commit on the given project.
    :param key: They key of the source for this update.
    :param owner: The owner of the repository.
    :param repository_name: The name of the project to update.
    :param commit_hash: The hash of the commit with which to associate the build.
    """
    state = request.args.get("state", default=BuildStatusStates.SUCCESSFUL)
    name = request.args.get("name")
    url = request.args.get("url")
    description = request.args.get("description")

    BuildStatus.create_buildstatus(
      owner=owner,
      repository_name=repository_name,
      revision=commit_hash,
      key=key,
      state=state,
      url=url,
      name=name,
      description=description,
      client=client
    )

    return {
        "commit": commit_hash,
        "owner": owner,
        "repository": repository_name,
        "status": "Build status updated successfully.",
    }


@app.route("/codeship/webhook", methods=["POST"])
@produces_json
def codeship_webhook():
    """
    A webhook for Codeship that automatically updates build status on BitBucket.
    """
    body = request.json
    build = body["build"]
    commit_hash = build["commit_id"]
    committer = build["committer"]
    description = build["message"]
    (owner, repository_name) = split_project_name(build["project_name"])
    status = convert_codeship_status(build["status"])
    url = build["build_url"]

    BuildStatus.create_buildstatus(
      owner=owner,
      repository_name=repository_name,
      revision=commit_hash,
      key='codeship',
      state=status,
      url=url,
      name=committer,
      description=description,
      client=client
    )

    return {
        "commit": commit_hash,
        "owner": committer,
        "repository": repository_name,
        "status": "Build status updated successfully.",
    }


if __name__ == "__main__":
    app.run(
      # support debug/hot-reload
      debug=bool(os.environ.get("DEBUG", True)),
      # bind to all interfaces
      host="0.0.0.0",
      # bind to the appropriate port (for Heroku/etc).
      port=int(os.environ.get("PORT", 4567))
    )
