# BitBucket Build Status

A small utility to publish build status information to the BitBucket API.

Also supports a webhook from Codeship to add build status information directly to BitBucket commits.
