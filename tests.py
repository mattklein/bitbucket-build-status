"""
Unit tests for the application and utilities.
"""

import unittest

import main
from utils import *


class IntegrationTestCase(unittest.TestCase):
    """
    Base class for any tests in this file.
    """

    def setUp(self):
        """
        Initializes a test client for the integration test.
        """
        self.app = main.app.test_client()

    def assertResponse(self, code, response):
        """
        Ensures the given response is of the given status.
        :param code: The expected response code.
        :param response: The response object to verify.
        """
        self.assertIsNotNone(response)
        self.assertEqual(code, response.status_code)


class BitBucketIntegrationTests(IntegrationTestCase):
    """
    General BitBucket integration tests.
    """

    # a dummy URI for build status tests
    BUILD_STATUS_URI = "/codeship/update/{project}/{commit_hash}?name=xeusalmighty".format(
      project="mattklein/bitbucket-build-status",
      commit_hash="2a5ea38287ab22cea142545d9f704f332fd78c51"
    )

    def test_update_build_status_succeeds_for_well_known_repository(self):
        """
        We should be able to invoke the update command and receive a 200 success.
        """
        self.assertResponse(200, self.app.put(self.BUILD_STATUS_URI))

    def test_update_build_status_accepts_only_put_request(self):
        """
        Only PUT requests are supported for general updates.
        """
        self.assertResponse(405, self.app.get(self.BUILD_STATUS_URI))
        self.assertResponse(405, self.app.post(self.BUILD_STATUS_URI))
        self.assertResponse(200, self.app.put(self.BUILD_STATUS_URI))
        self.assertResponse(405, self.app.patch(self.BUILD_STATUS_URI))


class CodeshipIntegrationTests(IntegrationTestCase):
    """
    Codeship Webhook integration tests.
    """

    # a dummy payload from the codeship webhook
    CODESHIP_DUMMY_PAYLOAD = """{
      "build": {
        "build_url": "https://codeship.com/projects/132897/builds/11932193",
        "commit_url": "https://bitbucket.org/mattklein/bitbucket-build-status/commits/8d66a78bee3c47f0fafe28b54c8fe4a5aaeea3ed",
        "project_id": 132897,
        "build_id": 11932193,
        "status": "success",
        "project_name": "mattklein/bitbucket-build-status",
        "project_full_name": "mattklein/bitbucket-build-status",
        "commit_id": "8d66a78bee3c47f0fafe28b54c8fe4a5aaeea3ed",
        "short_commit_id": "8d66a",
        "message": "Add status badge to README.md.",
        "committer": "mattklein",
        "branch": "master"
      }
    }"""

    def test_codeship_webhook_accepts_expected_payload(self):
        """
        A typical Codeship webhook payload should be understood by the system.
        """
        self.assertResponse(200, self.app.post(
          "/codeship/webhook",
          data=self.CODESHIP_DUMMY_PAYLOAD,
          content_type="application/json"
        ))

    def test_codeship_webhook_accepts_only_post_request(self):
        """
        Only POST requests are supported for the Codeship webhook.
        """
        self.assertResponse(405, self.app.get("/codeship/webhook"))
        self.assertResponse(200, self.app.post(
          "/codeship/webhook",
          data=self.CODESHIP_DUMMY_PAYLOAD,
          content_type="application/json"
        ))
        self.assertResponse(405, self.app.get("/codeship/webhook"))
        self.assertResponse(405, self.app.get("/codeship/webhook"))


class UtilsTest(unittest.TestCase):
    """
    General tests for the utilities.
    """

    def test_produces_json_wraps_result_in_response_object(self):
        """
        The helper should wrap the response in a Flask Response object with JSON encoded body.
        """
        self.assertIsInstance(self.json_producer(), main.Response)

    def test_split_project_name_yields_2_tuple(self):
        """
        The helper should split canonical project names into owner and repository parts.
        """
        (owner, project) = split_project_name("owner/project")
        self.assertEqual("owner", owner)
        self.assertEqual("project", project)

    def test_convert_codeship_status_accepts_known_states(self):
        """
        The set of known Codeship status codes should all be convertible.
        """
        self.assertEqual("INPROGRESS", convert_codeship_status("testing"))
        self.assertEqual("INPROGRESS", convert_codeship_status("waiting"))
        self.assertEqual("SUCCESSFUL", convert_codeship_status("success"))
        self.assertEqual("FAILURE", convert_codeship_status("infrastructure_failure"))
        self.assertEqual("FAILURE", convert_codeship_status("ignored"))
        self.assertEqual("FAILURE", convert_codeship_status("blocked"))
        self.assertEqual("FAILURE", convert_codeship_status("stopped"))

    def test_convert_codeship_status_is_case_insensitive(self):
        """
        Status code conversion should ignore case.
        """
        self.assertEqual("INPROGRESS", convert_codeship_status("TESTING"))
        self.assertEqual("FAILURE", convert_codeship_status("InFRAstructure_FailURE"))

    @produces_json
    def json_producer(self):
        return {
            "key": "value"
        }


if __name__ == "__main__":
    unittest.main()
