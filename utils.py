"""
General utilities for the application.
"""

from functools import wraps

from flask import Response, json


def produces_json(target):
    """
    Wraps a method and converts the result of that method to JSON and sets the response type as application/json.
    :param target: The target method to wrap.
    """

    @wraps(target)
    def decorated_function(*args, **kwargs):
        result = target(*args, **kwargs)
        response = json.dumps(result)

        return Response(response, content_type="application/json")

    return decorated_function


def split_project_name(project_name):
    """
    Takes a project name separated by a '/' character and returns a 2-tuple of owner/repository.
    :param project_name: The name of the project to split. Expected to be of the format owner/repository.
    """
    return project_name.split('/')


def convert_codeship_status(string):
    """
    Takes a status string from Codeship and converts it to something BitBucket would understand.
    :param string: The string to convert.
    """
    return {
        'testing': 'INPROGRESS',
        'waiting': 'INPROGRESS',
        'success': 'SUCCESSFUL',
        'error': 'FAILURE',
        'infrastructure_failure': 'FAILURE',
        'ignored': 'FAILURE',
        'blocked': 'FAILURE',
        'stopped': 'FAILURE',
    }[string.lower()]
